﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class EnnemisService
    {
        public IList<EnnemisDto> GetAllEnnemis()
        {
            using (EnnemisRepository _ennemisRepo = new EnnemisRepository())
            {
                return _ennemisRepo.GetAllEnnemis();
            }
        }

        public EnnemisDto AddEnnemis(EnnemisDto ennemis)
        {
            if (ennemis != null)
            {
                using (EnnemisRepository _ennemisRepo = new EnnemisRepository())
                {
                    return _ennemisRepo.AddEnnemis(ennemis);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(ennemis), "Can't be null");
            }
        }
    }
}
