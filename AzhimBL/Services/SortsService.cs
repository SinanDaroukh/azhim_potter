﻿using AzhimDAL.Repositories;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimBL.Services
{
    public class SortsService
    {
        public IList<SortsDto> GetAll()
        {
            using (SortsRepository _statsRepo = new SortsRepository())
            {
                return _statsRepo.GetAllSorts();
            }
        }

        public SortsDto Add(SortsDto sortsDto)
        {
            if (sortsDto != null)
            {
                using (SortsRepository _statRepo = new SortsRepository())
                {
                    return _statRepo.AddSorts(sortsDto);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(sortsDto), "Sorts can't be null");
            }
        }
    }
}
