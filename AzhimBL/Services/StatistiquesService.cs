﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Repositories;

namespace AzhimBL.Services
{
    public class StatistiquesService
    {
        public IList<StatistiquesDto> GetAll()
        {
            using (StatistiquesRepository _statsRepo = new StatistiquesRepository())
            {
                return _statsRepo.GetAllStatistiques();
            }
        }

        public StatistiquesDto Add(StatistiquesDto statistiques)
        {
            if (statistiques != null)
            {
                using (StatistiquesRepository _statRepo = new StatistiquesRepository())
                {
                    return _statRepo.AddStatistiques(statistiques);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(statistiques), "Statistiques can't be null");
            }
        }

        public void Remove(StatistiquesDto statistiques)
        {
            if (statistiques != null)
            {
                using (StatistiquesRepository _statRepo = new StatistiquesRepository())
                {
                    _statRepo.RemoveStatistiques(statistiques);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(statistiques), "Statistiques can't be null");
            }
        }
    }
}
