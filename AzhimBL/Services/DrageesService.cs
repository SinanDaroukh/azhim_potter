﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class DrageesService
    {
        public IList<DrageesDto> GetAllDragees()
        {
            using (DrageesRepository _drageesRepo = new DrageesRepository())
            {
                return _drageesRepo.GetAllDragees();
            }
        }

        public DrageesDto AddDragees(DrageesDto dragees)
        {
            if (dragees != null)
            {
                using (DrageesRepository _baguettesRepo = new DrageesRepository())
                {
                    return _baguettesRepo.AddDragees(dragees);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(dragees), "Can't be null");
            }
        }
    }
}
