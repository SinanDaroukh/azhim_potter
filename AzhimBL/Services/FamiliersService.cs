﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class FamiliersService
    {
        public IList<FamiliersDto> GetAllFamiliers()
        {
            using (FamiliersRepository _familiersRepo = new FamiliersRepository())
            {
                return _familiersRepo.GetAllFamiliers();
            }
        }

        public FamiliersDto AddFamiliers(FamiliersDto familiers)
        {
            if (familiers != null)
            {
                using (FamiliersRepository _familiersRepo = new FamiliersRepository())
                {
                    return _familiersRepo.AddFamiliers(familiers);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(familiers), "Can't be null");
            }
        }
    }
}
