﻿using AzhimDAL.Repositories;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimBL.Services
{
    public class TextesService
    {
        public IList<TextesDto> GetAll()
        {
            using (TextesRepository _textesRepo = new TextesRepository())
            {
                return _textesRepo.GetAllTextes();
            }
        }

        public TextesDto Add(TextesDto textes)
        {
            if (textes != null)
            {
                using (TextesRepository _texteRepo = new TextesRepository())
                {
                    return _texteRepo.AddTextes(textes);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(textes), "Textes can't be null");
            }
        }
    }
}
