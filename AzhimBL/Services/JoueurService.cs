﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class JoueurService
    {
        public JoueurDto GetJoueur()
        {
            using (JoueurRepository _joueurRepo = new JoueurRepository())
            {
                return _joueurRepo.GetJoueur();
            }
        }

        public JoueurDto AddJoueur(JoueurDto joueur)
        {
            if (joueur != null)
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.AddJoueur(joueur);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(joueur), "Can't be null");
            }
        }
    }
}
