﻿using AzhimDAL.Repositories;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimBL.Services
{
    public class RumeursService
    {
        public IList<RumeursDto> GetAll()
        {
            using (RumeursRepository _rumeursRepo = new RumeursRepository())
            {
                return _rumeursRepo.GetAllRumeurs();
            }
        }

        public RumeursDto Add(RumeursDto rumeursDto)
        {
            if (rumeursDto != null)
            {
                using (RumeursRepository _rumeursRepo = new RumeursRepository())
                {
                    return _rumeursRepo.AddRumeurs(rumeursDto);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(rumeursDto), "Rumeurs can't be null");
            }
        }
    }
}
