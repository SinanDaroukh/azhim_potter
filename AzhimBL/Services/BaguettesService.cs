﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class BaguettesService
    {
        public IList<BaguettesDto> GetAllBaguettes()
        {
            using (BaguettesRepository _baguettesRepo = new BaguettesRepository())
            {
                return _baguettesRepo.GetAllBaguettes();
            }
        }

        public BaguettesDto AddBaguette(BaguettesDto baguette)
        {
            if (baguette != null)
            {
                using (BaguettesRepository _baguettesRepo = new BaguettesRepository())
                {
                    return _baguettesRepo.AddBaguettes(baguette);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(baguette), "Can't be null");
            }
        }
    }
}
