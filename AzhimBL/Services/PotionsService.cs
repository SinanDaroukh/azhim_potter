﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDAL.Repositories;
using AzhimDTO.DTO;

namespace AzhimBL.Services
{
    public class PotionsService
    {
        public IList<PotionsDto> GetAllPotions()
        {
            using (PotionsRepository _potionsRepo = new PotionsRepository())
            {
                return _potionsRepo.GetAllPotions();
            }
        }

        public PotionsDto AddPotions(PotionsDto potions)
        {
            if (potions != null)
            {
                using (PotionsRepository _potionsRepo = new PotionsRepository())
                {
                    return _potionsRepo.AddPotions(potions);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(potions), "Can't be null");
            }
        }
    }
}
