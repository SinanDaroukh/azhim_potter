# Fichier lié au graphe

## Salle commune
### [1] - Parler à un camarade<br>
Un camarade aléatoire apparaît :
#### Gryffondor
 - Ron
 - Hermione
 - Neville
 - Félix
 - Mathis
 - JeanMich
#### Poufsouffle
 - Cédric Diggory
 - Hannah Abbot
 - Justin Finch-Fletchley
 - Jérémy
 - François
 - Dylan
#### Serdaigle
 - Cho Chang
 - Luna Lovegood
 - Terry Boot
 - Nada
 - Fongerlan
 - Julien koening
#### Serpentard
 - Drago Malefoy
 - Gregory Goyle
 - Pansy Parkinson
 - Sinan
 - Sylvain
 - Vincent

```
{camarade} est présent dans la salle commune, vous aller lui parler.
```
Aléatoire :
 - Le camarade lui apprend un sort (1/4)

```
Vous discutez un peu et {camarade} vous apprend le nouveau sort appris au dernier cours de sortilège (que vous avez seché).
```
 - Le camarade lui augmente une stat aléatoire (3/4)

```
Vous discutez un peu et {camarade} et sa sagesse vous augmente votre {stat} de {2,3,4}.
```


### [2] - Se reposer dans son lit
```
Vous faites une petite sieste de 12h et regagnez {1/2/3} points de vie.
```

### [3] - Fouiller les tiroirs de ses camarades
Probabilité de trouver des dragées -> 1, 2, 3 ou 4 avec probas gausse<br>
Ces probas sont augmentées par l'esquive (la gausse se déplace vers le 4)
```
Vous fouillez dans les tiroirs de vos camarades de chambre (et confirmez tous les clichés sur vous). Vous trouvez {1,2,3,4} dragées de Bertie Crochue !
```

### [4] - Aller voir Dumbledore
Probabilité de trouver Dumbledore à son bureau
 - il y est (1/2)
```
Dumbledore vous reçoit dans son bureau et vous montre quelques techniques puuisque vous êtes son élève préféré. Votre magie et votre précision augmente {2,3}.
```

### [5] - Aller voir Hagrid
5 1ere fois:
```
Tu es un sorcier Azhim !
```
6ème fois:
Vous obtenez un hypogriffe
```
Est-ce que tu veux bien t'occuper de Buck Azhim ?
```

### [6] - Uber Eats
```
Vous livrez des Chocogrenouilles et Patacitrouille dans tout Poudlard et obtenez {10,12,15} gallions.
```

## Exploration des sous-sols
### [7] - Weasley -> Acheter dragées
```
Vous achetez 3 dragées de Bertie Crochue pour 1 Gallion, une bonne affaire.
```

### [8] - Weasley -> Acheter calendrier des filles
Disponible uniquement si toutes les dragées ont été achetées.
```
Leur stock est épuisé mais les Weasley vous donne en compensation le calendrier des filles de Poudlard ;-).
```

### [9] - Se battre contre des créatures
Tirage aléatoire entre
 - 3 crabes de feu échappés d'Hagrid
 - quelques botruc
 - un essaim de doxy
 - des Pitiponk
```
Vous tombez nez à nez avec {créatures}.
```
 -> COMBAT ?

## Cours
### [10] - Botanique
Obtention d'une plante aléatoire:
 - une Branchiflore
 - un Bubobulb
 - un Filet du diable
 - une Mandragore
 - un Mimbulus mimbletonia
 - une Tentacula vénéneuse
```
Vous allez en cours de Botanique et à la fin du cours, Mme Chourave vous donne {plante}.
```

### [11] - Vol balai
Obtention d'un bonus permanent sur Uber eats (aléatoire {1, 2})
```
Vous allez en cours avec Mme. Bibine, votre habilité augmente, vous serez donc plus rapide sur un balai désormais !
```

### [12] - Défense contre les forces du mal
Tirage aléatoire de 2 bonus : courage et force
```
Vous allez en cours de Défense contre les forces du mal et vous vous armez contre les méchants et gagnez un bonus de {1,2,3} de force et un bonus de {1,2,3} de courage.
```

### [13] - Histoire de la magie
Tirage aléatoire bonus de savoir
```
Vous allez en cours d'Histoire de la magie et bien sûr M. Binns vous endort comme d'habitude mais vous bossez d'autres matières et obtenez un bonus de {1,2,3} en savoir.
```

### [14] - Potions
Obtention d'une potion aléatoire:
 - une potion de Goutte du mort-vivant
 - du Polynectar
 - un Philtre de confusion
 - du Felix Felicis
```
Vous allez en cours de potions et vous dérobez {potion} à la fin du cours.
```

### [15] - Métamorphose
Obtention d'un bonus d'esquive aléatoire
```
Vous allez en cours de Métamorphose et vous vous améliorez en tant qu'animagus et obtenez un bonus en esquive de {1,2,3}.
```

### [16] - Sortilège
Tirage aléatoire pour obtenir un nouveau sort
 - Expell