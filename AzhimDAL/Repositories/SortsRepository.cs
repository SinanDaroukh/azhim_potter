﻿using AzhimDAL.Extensions;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Repositories
{
    public class SortsRepository : BaseRepository
    {
        public SortsRepository() : base() { }
        public SortsRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<SortsDto> GetAllSorts()
        {
            List<Sorts> sortsEntities = _dbcontext.Sorts.ToList();
            return sortsEntities.Select(x => x.ToDto()).ToList();
        }

        public SortsDto AddSorts(SortsDto sortsDto)
        {
            Sorts sorts = sortsDto.ToEntity();
            _dbcontext.Sorts.Add(sorts);
            _dbcontext.SaveChanges();
            return sorts.ToDto();
        }
    }
}
