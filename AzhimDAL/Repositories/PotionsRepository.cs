﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class PotionsRepository : BaseRepository
    {
        public PotionsRepository() : base() { }
        public PotionsRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<PotionsDto> GetAllPotions()
        {
            List<Potions> potions = _dbcontext.Potions.ToList();
            return potions.Select(x => x.ToDto()).ToList();
        }

        public PotionsDto AddPotions(PotionsDto potionsDto)
        {
            Potions potions = potionsDto.ToEntity();
            _dbcontext.Potions.Add(potions);
            _dbcontext.SaveChanges();
            return potions.ToDto();
        }
    }
}
