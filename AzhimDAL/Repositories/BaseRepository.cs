﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Repositories
{
    public abstract class BaseRepository : IDisposable
    {
        protected readonly AzhimDBEntities _dbcontext = null;

        public BaseRepository()
        {
            _dbcontext = new AzhimDBEntities();
        }

        public BaseRepository(AzhimDBEntities dbContext)
        {
            _dbcontext = dbContext;
        }
        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
