﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class BaguettesRepository : BaseRepository
    {
        public BaguettesRepository() : base() { }
        public BaguettesRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<BaguettesDto> GetAllBaguettes()
        {
            List<Baguettes> baguettes = _dbcontext.Baguettes.ToList();
            return baguettes.Select(x => x.ToDto()).ToList();
        }

        public BaguettesDto AddBaguettes(BaguettesDto baguettesDto)
        {
            Baguettes baguettes = baguettesDto.ToEntity();
            _dbcontext.Baguettes.Add(baguettes);
            _dbcontext.SaveChanges();
            return baguettes.ToDto();
        }
    }
}
