﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class JoueurRepository : BaseRepository
    {
        public JoueurRepository() : base() { }
        public JoueurRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public JoueurDto GetJoueur()
        {
            Console.WriteLine(_dbcontext.Joueurs.ToList().Count());
            Joueurs joueur = _dbcontext.Joueurs.ToList()[0];
            return joueur.ToDto();
            //return null;
        }

        public JoueurDto AddJoueur(JoueurDto joueurDto)
        {
            Joueurs joueur = joueurDto.ToEntity();
            _dbcontext.Joueurs.Add(joueur);
            _dbcontext.SaveChanges();
            return joueur.ToDto();
        }
    }
}
