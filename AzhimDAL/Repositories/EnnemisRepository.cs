﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class EnnemisRepository : BaseRepository
    {
        public EnnemisRepository() : base() { }
        public EnnemisRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<EnnemisDto> GetAllEnnemis()
        {
            List<Ennemis> ennemis = _dbcontext.Ennemis.ToList();
            return ennemis.Select(x => x.ToDto()).ToList();
        }

        public EnnemisDto AddEnnemis(EnnemisDto ennemisDto)
        {
            Ennemis ennemis = ennemisDto.ToEntity();
            _dbcontext.Ennemis.Add(ennemis);
            _dbcontext.SaveChanges();
            return ennemis.ToDto();
        }
    }
}
