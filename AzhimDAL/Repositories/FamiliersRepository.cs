﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class FamiliersRepository : BaseRepository
    {
        public FamiliersRepository() : base() { }
        public FamiliersRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<FamiliersDto> GetAllFamiliers()
        {
            List<Familiers> familiers = _dbcontext.Familiers.ToList();
            return familiers.Select(x => x.ToDto()).ToList();
        }

        public FamiliersDto AddFamiliers(FamiliersDto familiersDto)
        {
            Familiers familiers = familiersDto.ToEntity();
            _dbcontext.Familiers.Add(familiers);
            _dbcontext.SaveChanges();
            return familiers.ToDto();
        }
    }
}
