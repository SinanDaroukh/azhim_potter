﻿using AzhimDAL.Extensions;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Repositories
{
    public class RumeursRepository : BaseRepository
    {
        public RumeursRepository() : base() { }

        public RumeursRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<RumeursDto> GetAllRumeurs()
        {
            List<Rumeurs> rumeursEntities = _dbcontext.Rumeurs.ToList();
            return rumeursEntities.Select(x => x.ToDto()).ToList();
        }

        public RumeursDto AddRumeurs(RumeursDto rumeursDto)
        {
            Rumeurs rumeurs = rumeursDto.ToEntity();
            _dbcontext.Rumeurs.Add(rumeurs);
            _dbcontext.SaveChanges();
            return rumeurs.ToDto();
        }

    }
}
