﻿using System.Collections.Generic;
using System.Linq;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class TextesRepository : BaseRepository
    {
        public TextesRepository() : base() { }
        public TextesRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<TextesDto> GetAllTextes()
        {
            List<Textes> textesEntities = _dbcontext.Textes.ToList();
            return textesEntities.Select(x => x.ToDto()).ToList();
        }

        public TextesDto AddTextes(TextesDto textesDto)
        {
            Textes textes = textesDto.ToEntity();
            _dbcontext.Textes.Add(textes);
            _dbcontext.SaveChanges();
            return textes.ToDto();
        }
    }
}
