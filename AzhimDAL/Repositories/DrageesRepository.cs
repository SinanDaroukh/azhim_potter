﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;
using AzhimDAL.Extensions;

namespace AzhimDAL.Repositories
{
    public class DrageesRepository : BaseRepository
    {
        public DrageesRepository() : base() { }
        public DrageesRepository(AzhimDBEntities dbContext) : base(dbContext) { }

        public List<DrageesDto> GetAllDragees()
        {
            List<Dragees> dragees = _dbcontext.Dragees.ToList();
            return dragees.Select(x => x.ToDto()).ToList();
        }

        public DrageesDto AddDragees(DrageesDto drageesDto)
        {
            Dragees dragees = drageesDto.ToEntity();
            _dbcontext.Dragees.Add(dragees);
            _dbcontext.SaveChanges();
            return dragees.ToDto();
        }
    }
}
