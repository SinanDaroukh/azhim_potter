﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzhimDAL.Extensions;
using AzhimDTO.DTO;

namespace AzhimDAL.Repositories
{
    public class StatistiquesRepository : BaseRepository
    {
        public StatistiquesRepository() : base() {}
        public StatistiquesRepository(AzhimDBEntities dbContext) : base(dbContext){}

        public List<StatistiquesDto> GetAllStatistiques()
        {
            List<Statistiques> statistiquesEntities = _dbcontext.Statistiques.ToList();
            return statistiquesEntities.Select(x => x.ToDto()).ToList();
        }

        public StatistiquesDto AddStatistiques(StatistiquesDto statistiquesDto)
        {
            Statistiques stats = statistiquesDto.ToEntity();
            _dbcontext.Statistiques.Add(stats);
            _dbcontext.SaveChanges();
            return stats.ToDto();
        }

        public void RemoveStatistiques(StatistiquesDto statistiques)
        {
            _dbcontext.Statistiques.Remove(_dbcontext.Statistiques.Single(a => a.Id == statistiques.Id));
            _dbcontext.SaveChanges();
        }
    }
}
