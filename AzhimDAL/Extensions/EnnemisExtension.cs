﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class EnnemisExtension
    {
        public static Ennemis ToEntity(this EnnemisDto ennemisDto)
        {
            return new Ennemis
            {
                Id = ennemisDto.Id,
                BaguetteID = ennemisDto.BaguetteID,
                RatioID = ennemisDto.RatioID,
                Vie = ennemisDto.Vie,
                Nom = ennemisDto.Nom,
                Infos = ennemisDto.Infos
            };
        }

        public static EnnemisDto ToDto(this Ennemis ennemis)
        {
            if (ennemis != null)
            {
                return new EnnemisDto()
                {
                    Id = ennemis.Id,
                    BaguetteID = ennemis.BaguetteID,
                    RatioID = ennemis.RatioID,
                    Vie = ennemis.Vie,
                    Nom = ennemis.Nom,
                    Infos = ennemis.Infos
                };
            }
            return null;
        }
    }
}
