﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class DrageesExtension
    {
        public static Dragees ToEntity(this DrageesDto baguettesDto)
        {
            return new Dragees
            {
                Id = baguettesDto.Id,
                Nom = baguettesDto.Nom,
                Descr = baguettesDto.Descr,
                RatioID = baguettesDto.RatioID
            };
        }

        public static DrageesDto ToDto(this Dragees baguettes)
        {
            if (baguettes != null)
            {
                return new DrageesDto()
                {
                    Id = baguettes.Id,
                    Nom = baguettes.Nom,
                    Descr = baguettes.Descr,
                    RatioID = baguettes.RatioID
                };
            }
            return null;
        }
    }
}
