﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class PotionsExtension
    {
        public static Potions ToEntity(this PotionsDto potionsDto)
        {
            return new Potions
            {
                Id = potionsDto.Id,
                Nom = potionsDto.Nom,
                Descr = potionsDto.Descr,
                RatioID = potionsDto.RatioID
            };
        }

        public static PotionsDto ToDto(this Potions potions)
        {
            if (potions != null)
            {
                return new PotionsDto()
                {
                    Id = potions.Id,
                    Nom = potions.Nom,
                    Descr = potions.Descr,
                    RatioID = potions.RatioID
                };
            }
            return null;
        }
    }
}
