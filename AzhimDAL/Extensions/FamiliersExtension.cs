﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class FamiliersExtension
    {
        public static Familiers ToEntity(this FamiliersDto familiersDto)
        {
            return new Familiers
            {
                Id = familiersDto.Id,
                Nom = familiersDto.Nom,
                Descr = familiersDto.Descr,
                RatioID = familiersDto.RatioID
            };
        }

        public static FamiliersDto ToDto(this Familiers familiers)
        {
            if (familiers != null)
            {
                return new FamiliersDto()
                {
                    Id = familiers.Id,
                    Nom = familiers.Nom,
                    Descr = familiers.Descr,
                    RatioID = familiers.RatioID
                };
            }
            return null;
        }
    }
}
