﻿using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Extensions
{
    public static class RumeursExtension
    {
        public static Rumeurs ToEntity(this RumeursDto rumeursDto)
        {
            return new Rumeurs
            {
                Id = rumeursDto.Id,
                TexteID = rumeursDto.TexteID,
                EnnemiID = rumeursDto.EnnemiID
            };
        }

        public static RumeursDto ToDto(this Rumeurs rumeurs)
        {
            if (rumeurs != null)
            {
                return new RumeursDto()
                {
                    Id = rumeurs.Id,
                    TexteID = rumeurs.TexteID,
                    EnnemiID = rumeurs.EnnemiID
                };
            }
            return null;
        }
    }
}
