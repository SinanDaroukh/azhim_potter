﻿using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Extensions
{
    public static class SortsExtension
    {
        public static Sorts ToEntity(this SortsDto textesDto)
        {

            return new Sorts
            {
                Id = textesDto.Id,
                Nom = textesDto.Nom,
                Descr = textesDto.Descr,
                RatioID = textesDto.RatioID
            };
        }

        public static SortsDto ToDto(this Sorts textes)
        {

            if (textes != null)
            {
                return new SortsDto()
                {
                    Id = textes.Id,
                    Nom = textes.Nom,
                    Descr = textes.Descr,
                    RatioID = textes.RatioID
                };
            }
            return null;

        }
    }
}
