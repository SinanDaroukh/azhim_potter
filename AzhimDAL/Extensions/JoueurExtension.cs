﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class JoueurExtension
    {
        public static Joueurs ToEntity(this JoueurDto joueurDto)
        {
            return new Joueurs
            {
                Id = joueurDto.Id,
                BaguetteID = joueurDto.BaguetteID,
                FamilierID = joueurDto.FamilierID,
                RatioID = joueurDto.RatioID,
                Hypogriffe = joueurDto.Hypogriffe,
                Vie = joueurDto.Vie
            };
        }

        public static JoueurDto ToDto(this Joueurs joueur)
        {
            if (joueur != null)
            {
                return new JoueurDto()
                {
                    Id = joueur.Id,
                    BaguetteID = joueur.BaguetteID,
                    FamilierID = joueur.FamilierID,
                    RatioID = joueur.RatioID,
                    Hypogriffe = joueur.Hypogriffe,
                    Vie = joueur.Vie,
                    Baguette = joueur.Baguettes.ToDto(),
                    Stats = joueur.Statistiques.ToDto(),
                    Fam = joueur.Familiers.ToDto()
                };
            }
            return null;
        }
    }
}
