﻿using AzhimDAL.Repositories;
using AzhimDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDAL.Extensions
{
    public static class TextesExtension
    {
        public static Textes ToEntity(this TextesDto textesDto)
        {
            Textes textes = new Textes();

            textes.Texte = textesDto.Texte;

            return textes;
        }

        public static TextesDto ToDto(this Textes textes)
        {
            TextesDto textesDto = new TextesDto();

            return textesDto;

        }

    }
}
