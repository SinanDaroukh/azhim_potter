﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class BaguettesExtension
    {
        public static Baguettes ToEntity(this BaguettesDto baguettesDto)
        {
            return new Baguettes
            {
                Id = baguettesDto.Id,
                Nom = baguettesDto.Nom,
                Descr = baguettesDto.Descr,
                Type = baguettesDto.Type,
                RatioID = baguettesDto.RatioID
            };
        }

        public static BaguettesDto ToDto(this Baguettes baguettes)
        {
            if (baguettes != null)
            {
                return new BaguettesDto()
                {
                    Id = baguettes.Id,
                    Nom = baguettes.Nom,
                    Descr = baguettes.Descr,
                    Type = baguettes.Type,
                    RatioID = baguettes.RatioID
                };
            }
            return null;
        }
    }
}
