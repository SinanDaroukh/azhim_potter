﻿using AzhimDTO.DTO;

namespace AzhimDAL.Extensions
{
    public static class StatistiquesExtension
    {
        public static Statistiques ToEntity(this StatistiquesDto statistiquesDto)
        {
            return new Statistiques
            {
                Id = statistiquesDto.Id,
                Attaque = statistiquesDto.Attaque,
                Defense = statistiquesDto.Defense,
                Force = statistiquesDto.Force,
                Précision = statistiquesDto.Précision,
                Rapidité = statistiquesDto.Rapidité,
                Magie = statistiquesDto.Magie,
                Esquive = statistiquesDto.Esquive,
                Endurance = statistiquesDto.Endurance,
                DefenseMagique = statistiquesDto.DefenseMagique
            };
        }

        public static StatistiquesDto ToDto(this Statistiques statistiques)
        {
            if (statistiques != null)
            {
                return new StatistiquesDto()
                {
                    Id = statistiques.Id,
                    Attaque = statistiques.Attaque,
                    Defense = statistiques.Defense,
                    Force = statistiques.Force,
                    Précision = statistiques.Précision,
                    Rapidité = statistiques.Rapidité,
                    Magie = statistiques.Magie,
                    Esquive = statistiques.Esquive,
                    Endurance = statistiques.Endurance,
                    DefenseMagique = statistiques.DefenseMagique
                };
            }
            return null;
        }

    }
}
