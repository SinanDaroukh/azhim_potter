//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AzhimDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Joueurs
    {
        public int Id { get; set; }
        public int BaguetteID { get; set; }
        public Nullable<int> FamilierID { get; set; }
        public int RatioID { get; set; }
        public int Hypogriffe { get; set; }
        public int Vie { get; set; }
    
        public virtual Baguettes Baguettes { get; set; }
        public virtual Familiers Familiers { get; set; }
        public virtual Statistiques Statistiques { get; set; }
    }
}
