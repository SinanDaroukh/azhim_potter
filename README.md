# Services .NET C#

Dans ce jeu, vous incarnez Azhim Potter, l'alter égo oriental d'Harry Potter. Le but est de vivre l'aventure Poudlard et de terrasser la terrible Voldebrol, une Madame Chabrol corrumpu par le mal
###### (N.B. Nous apprécions fortement Mme Chabrol, c'est purement humoristique, dans la vraie vie, c'est une professeure adorable ) ! 

## Introduction

#### Étape 1 : Répartition des stats du joueur

Voici les stats de base de notre joueur :
- Attaque (+10)
- Defense (+10)
- Force (+10)
- Précision (+10)
- Rapidité (+10)
- Magie (+10)
- Esquive (+10)
- Endurance (+10)
- Defense magique (+10)

En plus de ces statistiques de bases, le joueur doit distribuer 10 points supplémentaires.

#### Étape 2 : Choix de la baguette

Le joueur devra choisir 1 baguette parmi 3 baguettes les plus basiques de chaque type.
Une baguette a un type : 
Type de baguette :
1. Crin de Licorne : + Magie, + Défense Magique
2. Ventricule de Dragon : + Force, + Attaque
3. Plume de Phoenix : + Rapidite, + Précision

Le bonus des stats dépendent des spécs de la baguette qui seront randomisés.

Les baguettes implementent un système de triangle des armes : (°) => Crin de Licorne => Ventricule de Dragon => Plume de Phoenix => (°)
Cela modifiera le multiplicateur de dégats en fonction de la baguette de l'opposant du protagoniste.
(Il sera possible de glaner des informations sur les semi-boss à la bibliothèque)

Le joueur est donc libre de choisir sa baguette, et donc le type avec lequel il souhaite commencer le jeu.

#### Étape 3 : Choix du compagnon

Le héros a la possibilité de faire une promenade pour se détendre et découvrir les environs du château de Poudlard en fonction du chemin choisi, il obtient un animal en particulier.
L'animal donnera des bonus de stats. Il est possible néanmoins de ne pas partir en balade et ne pas bénéficier de compagnon au long de l’aventure.

Il y a donc 3 familiers :
1. Chat (+ )
2. Chouette (+ )
3. Rat (+ )

#### Étape 4 : L'attribution de la maison (F1, F2, F3, F5)

Une série de questions posées qui permettent de connaître la maison du héros. La maison change les affinités avec les autres élèves de l'école. 
La maison donne aussi une action spécifique à réaliser lors du cycle des journées !

## Commencement du cycle des journées

Le cycle des journées est la boucle de jeu principale. Tout les 2/3 jours, un mini-boss attaque Poudlard, le joueur se retrouve confronter à ce dernier et doit le défaire !
Au bout d’un certain nombre de journées, la phase finale du jeu s'amorce et le joueur devra combattre Voldebrol !

Pour se préparer, le joueur possède 3 points d'actions qu'il peut dépenser à sa guise, chaque journée, parmi la liste des actions suivantes :

#### Action : Procrastiner dans les chambres de la maison et dans Poudlard !

1. Parler à un camarade sorcier de la  maison
	- Cette action octroie un gain de stats, ou permet d'apprendre un sort en fonction du score au jet de dès. Les camarades étant différents, les sorts appris le seront aussi !
2. Se reposer dans son lit
	- Cette action permet au joueur de regagner des PV
3. Fouiller dans les tiroirs de ses camarades
	- Cette action permet au joueur de dérober des dragées dans les tiroirs de ses camarades, et potentiellement de gagner de l'argent. 
	- Cette action est régit par le score au jet de dès basé sur l'Esquive.
4. Aller voir Albus Dumbledore
	- Cette action octroie au joueur un buff de Magie et de Précision
	- Sauf qu'il ne sera pas toujours pas là, jet de chance pour pouvoir le rencontrer
5. Aller voir Hagrid 
	- Cette action ne sert à rien, mis à part, (re)découvrir une des fameuses répliques de Hagrid : "Tu es un sorcier, Azhim !"
	- Sauf si vous allez le voir 6 fois, vous obtiendrez Buck, un familier qui buff le gain d'argent, et balance 5 dégats à chaque début de combat !
6. UberEats
    - Livrer des chocogrenouilles en balais et d'autres bouffes !

#### Action : Crapaüter dans les sous-sols

1. Weasley & Co
	-  Les frères Weasley tiennent une échoppe, où ils y vendent des dragées qui peuvent être utilisées en combat et donner des buffs quelconques. 
	-  La légende raconte qu'ils vendraient aussi un calendrier des filles de Poudlard version non censurée, cet item est déblocable une fois toutes les dragées achetées. (Et il ne sert à rien)

2. Se battre contre quelconques créatures
    - Permet de gagner de l'expérience, qui permettra d'augmenter son niveau et ses stats

#### Action : Aller en cours

Choix de la matière parmi une liste proposée :

- Botanique (Obtention de plantes à effet) (Sort à usage unique)
- Défense contre les forces du mal (Courage, Force)
- Histoire de la magie (Savoir)
- Métamorphose (Esquive)
- Potions (Obtention de potions à effet -> jet de chance)
- Sortilèges (Apprentissage d’un nouveau sort -> jet de chance)
- Vol sur balai (Rapidité en vol, plus d’argent généré)

#### Action : Aller à la bibliothèque

Si la bibliothèquaire est présente, on se rend automatiquement à la Bibliothèque Classique
Sinon, on pourra se rendre à la Réserve ! La bibliothéquaire est là : 66% !

1. Bibliothèque Classique :
    - La biblio classique octroie un gain de savoir conséquent mais moins important que le cours d'Histoire de la Magie
2. Bibliothèque Réserve :
    - Permet de glaner des informations sur un des boss de la database, comme ses faiblesses et ses stats.


#### Action : Aller au Pré-au-Lard

1. Hiroyuki Bagetoru :
    - Permet d'acheter des baguettes

2. Les Trois Balais :
    - Entendre des rumeurs, et savoir qui attaque Poudlard au prochain cycle
    - Permet d'acheter des balais pour pratiquer le UberEats plus vite et gagner des sous

## Conclusion

La conclusion sonne le dernier combat contre Voldemort, le boss aura potentiellment plusieurs formes !

## Base de données

1000 : Baguette
2000 : Fam
3000 : Potions
4000 : Dragees
5000 : Enemis
6000 : Sorts
