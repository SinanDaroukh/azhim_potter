﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDTO.DTO
{
    public class JoueurDto
    {
        public JoueurDto()
        {

        }
        public int Id { get; set; }
        public int BaguetteID { get; set; }
        public Nullable<int> FamilierID { get; set; }
        public int RatioID { get; set; }
        public int Hypogriffe { get; set; }
        public int Vie { get; set; }

        public virtual BaguettesDto Baguette { get; set; }
        public virtual FamiliersDto Fam { get; set; }
        public virtual StatistiquesDto Stats { get; set; }
    }
}
