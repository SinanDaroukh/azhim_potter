﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDTO.DTO
{
    public class PotionsDto
    {
        public PotionsDto()
        {

        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Descr { get; set; }
        public int RatioID { get; set; }
    }
}
