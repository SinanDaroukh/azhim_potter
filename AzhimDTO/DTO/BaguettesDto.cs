﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDTO.DTO
{
    public class BaguettesDto
    {
        public BaguettesDto()
        {

        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Descr { get; set; }
        public string Type { get; set; }
        public int RatioID { get; set; }
    }
}
