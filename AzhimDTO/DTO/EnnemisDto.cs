﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDTO.DTO
{
    public class EnnemisDto
    {
        public EnnemisDto()
        {

        }

        public int Id { get; set; }
        public int BaguetteID { get; set; }
        public int RatioID { get; set; }
        public int Vie { get; set; }
        public string Nom { get; set; }
        public string Infos { get; set; }
    }
}
