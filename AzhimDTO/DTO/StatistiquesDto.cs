﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzhimDTO.DTO
{
    public class StatistiquesDto
    {
        public StatistiquesDto()
        {

        }

        public int GetStat(int index)
        {
            switch (index)
            {
                case 0:
                    return Attaque;
                case 1:
                    return Defense;
                case 2:
                    return Force;
                case 3:
                    return Précision;
                case 4:
                    return Rapidité;
                case 5:
                    return Magie;
                case 6:
                    return Esquive;
                case 7:
                    return Endurance;
                case 8:
                    return DefenseMagique;
                default:
                    return Id;
            }
        }

        public int Id { get; set; }
        public int Attaque { get; set; }
        public int Defense { get; set; }
        public int Force { get; set; }
        public int Précision { get; set; }
        public int Rapidité { get; set; }
        public int Magie { get; set; }
        public int Esquive { get; set; }
        public int Endurance { get; set; }
        public int DefenseMagique { get; set; }
    }
}
