using AzhimDTO.DTO;
using AzhimBL.Services;
using System;

namespace AzhimCMD
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Bienvenu chez Azhim :D");

            Console.WriteLine("Vos stats :");
            JoueurService jservice = new JoueurService();
            JoueurDto joueur = jservice.GetJoueur();

            StatistiquesDto statsJoueur = joueur.Stats;

            for (int i = 0; i < 9; i++)
            {
                Console.WriteLine(statsJoueur.GetStat(i));
            }
            Console.ReadLine();
            
            StatistiquesService service = new StatistiquesService();
            foreach (var s in service.GetAll())
            {
                Console.WriteLine(s.Id);
                service.Remove(s);
            }

            Console.ReadLine();
        }
    }
}
