﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AzhimAPI.ViewModel
{
    [DataContract]
    internal class TextesViewModel
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Texte { get; set; }
    }
}