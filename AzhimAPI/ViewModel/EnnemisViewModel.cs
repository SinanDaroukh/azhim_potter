﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AzhimAPI.ViewModel
{  
    [DataContract]
    internal class EnnemisViewModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BaguetteID { get; set; }
        [DataMember]
        public int RatioID { get; set; }
        [DataMember]
        public int Vie { get; set; }
        [DataMember]
        public string Nom { get; set; }
        [DataMember]
        public string Infos { get; set; }
    }
}