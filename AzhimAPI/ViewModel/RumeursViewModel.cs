﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AzhimAPI.ViewModel
{
    [DataContract]
    internal class RumeursViewModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int EnnemiID { get; set; }
        [DataMember]
        public string TexteID { get; set; }
    }
}