﻿using System;
using System.Runtime.Serialization;

namespace AzhimAPI.ViewModel
{
    [DataContract]
    internal class StatistiquesViewModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Attaque { get; set; }
        [DataMember]
        public int Defense { get; set; }
        [DataMember]
        public int Force { get; set; }
        [DataMember]
        public int Précision { get; set; }
        [DataMember]
        public int Rapidité { get; set; }
        [DataMember]
        public int Magie { get; set; }
        [DataMember]
        public int Esquive { get; set; }
        [DataMember]
        public int Endurance { get; set; }
        [DataMember]
        public int DefenseMagique { get; set; }
    }
}