﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AzhimAPI.ViewModel
{
    [DataContract]
    internal class JoueurViewModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BaguetteID { get; set; }
        [DataMember]
        public Nullable<int> FamilierID { get; set; }
        [DataMember]
        public int RatioID { get; set; }
        [DataMember]
        public int Hypogriffe { get; set; }
        [DataMember]
        public int Vie { get; set; }
        [DataMember]
        internal StatistiquesViewModel Stats { get; set; }
    }
}