﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AzhimAPI.ViewModel
{
    [DataContract]
    internal class PotionsViewModel
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nom { get; set; }
        [DataMember]
        public string Descr { get; set; }
        [DataMember]
        public int RatioID { get; set; }
    }
}