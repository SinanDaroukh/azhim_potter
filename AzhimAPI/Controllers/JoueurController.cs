﻿using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.ViewModel
{
    [Produces("application/json")]
    public class JoueurController : ApiController
    {
        private readonly JoueurService _joueursService = null;

        public JoueurController()
        {
            _joueursService = new JoueurService();
        }

        //// GET: api/joueurs
        [ResponseType(typeof(JoueurViewModel))]
        public IHttpActionResult Get()
        {
            JoueurDto joueur = _joueursService.GetJoueur();

            StatistiquesViewModel joueurStat = new StatistiquesViewModel() 
            { 
                Id = joueur.Stats.Id,
                Attaque = joueur.Stats.Attaque,
                Defense = joueur.Stats.Defense,
                Force = joueur.Stats.Force,
                Précision = joueur.Stats.Précision,
                Rapidité = joueur.Stats.Rapidité,
                Magie = joueur.Stats.Magie,
                Esquive = joueur.Stats.Esquive,
                Endurance = joueur.Stats.Endurance,
                DefenseMagique = joueur.Stats.DefenseMagique
            };
            JoueurViewModel joueurResult = new JoueurViewModel()
            {
                Id = joueur.Id,
                BaguetteID = joueur.BaguetteID,
                FamilierID = joueur.FamilierID,
                RatioID = joueur.RatioID,
                Hypogriffe = joueur.Hypogriffe,
                Vie = joueur.Vie,
                Stats = joueurStat

            };
            return Ok(joueurResult);
        }
    }
}
