﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class FamiliersController : ApiController
    {
        private readonly FamiliersService _familiersService = null;

        public FamiliersController()
        {
            _familiersService = new FamiliersService();
        }

        //// GET: api/familiers
        [ResponseType(typeof(IEnumerable<FamiliersViewModel>))]
        public IHttpActionResult Get()
        {
            IList<FamiliersDto> familiers = _familiersService.GetAllFamiliers();
            IEnumerable<FamiliersViewModel> familiersList = familiers.Select(x => new FamiliersViewModel
            {
                Id = x.Id,
                Descr = x.Descr,
                RatioID = x.RatioID,
                Type = x.Type,
                Nom = x.Nom
            });

            return Ok(familiersList);
        }
    }
}
