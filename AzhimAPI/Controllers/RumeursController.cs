﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{

        [Produces("application/json")]
        public class RumeursController : ApiController
        {
            private readonly RumeursService _rumeursService = null;

            public RumeursController()
            {
                _rumeursService = new RumeursService();
            }

            //// GET: api/rumeurs
            [ResponseType(typeof(IEnumerable<RumeursViewModel>))]
            public IHttpActionResult Get()
            {
                IList<RumeursDto> rumeurs = _rumeursService.GetAll();
                IEnumerable<RumeursViewModel> rumeursList = rumeurs.Select(x => new RumeursViewModel
                {
                    Id = x.Id,
                    EnnemiID = x.EnnemiID,
                    TexteID = x.TexteID
                });

                return Ok(rumeursList);
            }
        }
}
