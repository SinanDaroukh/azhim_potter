﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class PotionsController : ApiController
    {
        private readonly PotionsService _potionsService = null;

        public PotionsController()
        {
            _potionsService = new PotionsService();
        }

        //// GET: api/potions
        [ResponseType(typeof(IEnumerable<PotionsViewModel>))]
        public IHttpActionResult Get()
        {
            IList<PotionsDto> potions = _potionsService.GetAllPotions();
            IEnumerable<PotionsViewModel> potionsList = potions.Select(x => new PotionsViewModel
            {
                Id = x.Id,
                Descr = x.Descr,
                RatioID = x.RatioID,
                Nom = x.Nom
            });

            return Ok(potionsList);
        }
    }
}
