﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class TextesController : ApiController
    {
        private readonly TextesService _textesService = null;

        public TextesController()
        {
            _textesService = new TextesService();
        }

        //// GET: api/textes
        [ResponseType(typeof(IEnumerable<TextesViewModel>))]
        public IHttpActionResult Get()
        {
            IList<TextesDto> textes = _textesService.GetAll();
            IEnumerable<TextesViewModel> textesList = textes.Select(x => new TextesViewModel
            {
                Id = x.Id,
                Texte = x.Texte
            });

            return Ok(textesList);
        }
    }
}
