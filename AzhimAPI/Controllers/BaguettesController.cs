﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class BaguettesController : ApiController
    {
        private readonly BaguettesService _baguettesService = null;

        public BaguettesController()
        {
            _baguettesService = new BaguettesService();
        }

        //// GET: api/baguettes
        [ResponseType(typeof(IEnumerable<BaguettesViewModel>))]
        public IHttpActionResult Get()
        {
            IList<BaguettesDto> baguettes = _baguettesService.GetAllBaguettes();
            IEnumerable<BaguettesViewModel> baguettesList = baguettes.Select(bag => new BaguettesViewModel
            {
                Id = bag.Id,
                Nom = bag.Nom,
                Descr = bag.Descr,
                Type = bag.Type,
                RatioID = bag.RatioID
            });
            return Ok(baguettesList);

        }
    }
}
