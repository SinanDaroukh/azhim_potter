﻿using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.ViewModel
{
    [Produces("application/json")]
    public class StatistiquesController : ApiController
    {
        private readonly StatistiquesService _statistiquesService = null;

        public StatistiquesController()
        {
            _statistiquesService = new StatistiquesService();
        }

        //// GET: api/statistiques
        [ResponseType(typeof(IEnumerable<StatistiquesViewModel>))]
        public IHttpActionResult Get()
        {
            IList<StatistiquesDto> statistiques = _statistiquesService.GetAll();
            IEnumerable<StatistiquesViewModel> statsList = statistiques.Select(x => new StatistiquesViewModel
            {
                Id = x.Id,
                Attaque = x.Attaque,
                Defense = x.Defense,
                Force = x.Force,
                Précision = x.Précision,
                Rapidité = x.Rapidité,
                Magie = x.Magie,
                Esquive = x.Esquive,
                Endurance = x.Endurance,
                DefenseMagique = x.DefenseMagique
            });

            return Ok(statsList);
        }
    }
}
