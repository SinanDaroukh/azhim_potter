﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class EnnemisController : ApiController
    {
        private readonly EnnemisService _ennemisService = null;

        public EnnemisController()
        {
            _ennemisService = new EnnemisService();
        }

        //// GET: api/ennemis
        [ResponseType(typeof(IEnumerable<EnnemisViewModel>))]
        public IHttpActionResult Get()
        {
            IList<EnnemisDto> ennemis = _ennemisService.GetAllEnnemis();
            IEnumerable<EnnemisViewModel> ennemisList = ennemis.Select(x => new EnnemisViewModel
            {
                Id = x.Id,
                BaguetteID = x.BaguetteID,
                RatioID = x.RatioID,
                Vie = x.Vie,
                Nom = x.Nom,
                Infos = x.Infos
            });

            return Ok(ennemisList);
        }
    }
}
