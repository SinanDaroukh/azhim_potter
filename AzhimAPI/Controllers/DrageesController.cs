﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class DrageesController : ApiController
    {
        private readonly DrageesService _drageesService = null;

        public DrageesController()
        {
            _drageesService = new DrageesService();
        }

        //// GET: api/dragees
        [ResponseType(typeof(IEnumerable<DrageesViewModel>))]
        public IHttpActionResult Get()
        {
            IList<DrageesDto> dragees = _drageesService.GetAllDragees();
            IEnumerable<DrageesViewModel> drageesList = dragees.Select(x => new DrageesViewModel
            {
                Id = x.Id,
                Nom = x.Nom,
                Descr = x.Descr,
                RatioID = x.RatioID,

            });

            return Ok(drageesList);
        }
    }
}
