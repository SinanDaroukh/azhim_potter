﻿using AzhimAPI.ViewModel;
using AzhimBL.Services;
using AzhimDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AzhimAPI.Controllers
{
    [Produces("application/json")]
    public class SortsController : ApiController
    {
        private readonly SortsService _sortsService = null;

        public SortsController()
        {
            _sortsService = new SortsService();
        }

        //// GET: api/sorts
        [ResponseType(typeof(IEnumerable<SortsViewModel>))]
        public IHttpActionResult Get()
        {
            IList<SortsDto> sorts = _sortsService.GetAll();
            IEnumerable<SortsViewModel> sortsList = sorts.Select(x => new SortsViewModel
            {
                Id = x.Id,
                Descr = x.Descr,
                RatioID = x.RatioID,
                Nom = x.Nom,            
            });

            return Ok(sortsList);
        }
    }
}
